@extends('layouts.app')

@section('content')


<div class="container">

    @foreach ($posts as $post)

    <div class="card mb-12">

        <h5 class="card-header">{{ $post->name }}</h5>

        <div class="box-img">
            <img src="{{ $post->file }}" alt="">
        </div>

        <div class="card-body">
          <p class="card-text">{{ $post->excerpt }}</p>
        </div>

        <div class="card-body">
          <a href="{{ route('post', $post->slug) }}" class="card-link">Leer más</a>
        </div>


    </div>
    <br>
    @endforeach

    {{ $posts->render() }}







</div>





@endsection
