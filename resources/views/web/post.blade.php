@extends('layouts.app')

@section('content')


<div class="container">

    <h3>{{ $post->name }}</h3>

    <div class="card mb-12">

        <h5 class="card-header">Categoria <a href="{{ route('category', $post->category->slug) }}">{{ $post->category->name }}</a></h5>

        <div class="box-img">
            <img src="{{ $post->file }}" alt="">
        </div>

        <div class="card-body">
            <p class="card-text">{{ $post->excerpt }}</p>
            <hr>
            <p class="card-text">{!! $post->body !!}</p> {{-- al imprimir de esta forma interpreta el html guardado en la bbdd --}}
            <hr>
            @foreach ($post->tags as $tag)
                <a href="{{ route('tags', $tag->slug) }}">{{ $tag->name }}</a>
            @endforeach
        </div>

    </div>

</div>

@endsection
