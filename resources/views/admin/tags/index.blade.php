@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Lista de etiquestas
                    <a href="{{ route('tags.create') }}" style="float: right; border: solid .5px #596196!important;" class="btn btn-sm btn-primary">Nuevo</a>

                </div>


                <div class="card-body">

                    <table class="table table-hover">
                        <thead>
                            <tr>

                            <th width='400'>Nombre</th>
                            <th width='400'>Slug</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($tags as $tag)
                            <tr class="table-default">
                                <td width='300'>{{ $tag->name }}</td>
                                <td width='400'>{{ $tag->slug }}</td>
                                <td width="10px">
                                    <a href="{{ route('tags.show', $tag->id) }}" class="btn btn-sm btn-secondary">Ver</a>

                                </td>
                                <td width="10px">
                                    <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-sm btn-secondary">Editar</a>
                                </td>
                                <td width="10px">
                                    {{ Form::open(['route' => ['tags.destroy', $tag->id], 'method' => 'DELETE']) }}
                                        <button class="btn btn-outline-danger btn-sm">Eliminar</button>
                                    {{ Form::close() }}
                                </td>





                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{ $tags->render() }}
            </div>
        </div>
    </div>
</div>

@endsection
