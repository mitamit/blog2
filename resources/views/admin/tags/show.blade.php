@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h6 class="card-header">Ficha de la etiqueta</h6>

                <div class="card-body">

                    <p><b>Nombre: </b>{{ $tag->name }}</p>
                    <p><b>Slug: </b>{{ $tag->slug }}</p>


                </div>

            </div>
        </div>
    </div>
</div>

@endsection


