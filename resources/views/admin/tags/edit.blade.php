@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h6 class="card-header">Modificar etiqueta</h6>

                <div class="card-body">

                    {!! Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'PUT']) !!}
                        @include('admin.tags.partials.form')
                    {!! Form::close() !!}


                </div>

            </div>
        </div>
    </div>
</div>

@endsection


