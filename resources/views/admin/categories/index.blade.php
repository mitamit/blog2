@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Lista de categorías
                    <a href="{{ route('categories.create') }}" style="float: right; border: solid .5px #596196!important;" class="btn btn-sm btn-primary">Nuevo</a>

                </div>


                <div class="card-body">

                    <table class="table table-hover">
                        <thead>
                            <tr>

                            <th width='400'>Nombre</th>
                            <th width='400'>Slug</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($categories as $category)
                            <tr class="table-default">
                                <td width='300'>{{ $category->name }}</td>
                                <td width='400'>{{ $category->slug }}</td>
                                <td width="10px">
                                    <a href="{{ route('categories.show', $category->id) }}" class="btn btn-sm btn-secondary">Ver</a>

                                </td>
                                <td width="10px">
                                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-sm btn-secondary">Editar</a>
                                </td>
                                <td width="10px">
                                    {{ Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'DELETE']) }}
                                        <button class="btn btn-outline-danger btn-sm">Eliminar</button>
                                    {{ Form::close() }}
                                </td>





                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
                {{ $categories->render() }}
            </div>
        </div>
    </div>
</div>

@endsection
