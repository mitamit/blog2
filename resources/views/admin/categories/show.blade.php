@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Ficha
                </div>


                <div class="card-body">
                    <p><b>Nombre: </b>{{ $category->name }}</p>
                    <p><b>Slug: </b>{{ $category->slug }}</p>
                    <b>Descripción</b>
                    <p>{{ $category->body }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
