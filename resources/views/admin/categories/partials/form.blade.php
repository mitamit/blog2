<div class="form-group">
    {{ Form::label('name', 'Nombre') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
    <div>
        @if($errors->has('name'))
            <p class="error">{{ $errors->first('name') }}</p>
        @endif
    </div>

</div>

<div class="form-group">
    {{ Form::label('slug', 'Url amigable') }}
    {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
    <div>
        @if($errors->has('slug'))
            <p class="error">{{ $errors->first('slug') }}</p>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('body', 'Descripción') }}
    {{ Form::textarea('body', null, ['class' => 'form-control', 'rows' => '2']) }}
    <div>
        @if($errors->has('body'))
            <p class="error">{{ $errors->first('body') }}</p>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>


@section('scripts')

<script src="{{ asset('vendor/stringtoslug/jquery.stringtoslug.min.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#name, #slug').stringToSlug({
            callback: function(text){
                $('#slug').val(text);
            }

        });
    });
</script>



@endsection
